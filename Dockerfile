FROM alpine
MAINTAINER Jason Plum <jplum@gitlab.com>

ENV REFRESHED_AT="2018-04-18" \
    POWERDNS_VERSION=4.1.1 

RUN apk --no-cache add python openssl libstdc++ libgcc && \
    apk --no-cache add --virtual build-deps \
      g++ make curl boost-dev openssl-dev git && \
    curl -sSL https://downloads.powerdns.com/releases/pdns-$POWERDNS_VERSION.tar.bz2 | tar xj -C /tmp && \
    cd /tmp/pdns-$POWERDNS_VERSION && \
    ./configure --prefix="" --exec-prefix=/usr --sysconfdir=/etc/pdns \
      --with-modules="pipe" --with-dynmodules="" --without-lua && \
    make && make install-strip && cd / && \
    mkdir -p /etc/pdns/conf.d && \
    rm -rf /tmp/pdns-$POWERDNS_VERSION && \
    git clone https://xp-dev.com/git/nip.io /tmp/nip && \
    mkdir /nip && mv /tmp/nip/src/* /nip && rm -rf /tmp/nip && \
    addgroup -S pdns 2>/dev/null && \
    adduser -S -D -H -h /var/empty -s /bin/false -G pdns -g pdns pdns 2>/dev/null && \
    apk del --purge build-deps && \
    rm -rf /tmp/pdns-$POWERDNS_VERSION

ADD entrypoint.sh /

EXPOSE 53/tcp 53/udp

ENTRYPOINT ["/entrypoint.sh"]
