# PowerDNS + NIP.IO

This container provides a bare minimum installation of [PowerDNS][] combined with the [NIP.IO][] on top of Alpine. This then exposes port `53` over TCP and UDP.

## Configuration

This container is configured through two environment variables:
- `NIP_DOMAIN`: Set this to the top level domain you wish to use in combination with NIP. Defaults to `lcl.io`
- `NIP_DEFAULT_ADDRESS`: Set this to the IP address you wish any un-qualified host to resolve to. This defaults to `127.0.0.1`

## Usage

> *Notice:* The PowerDNS does not have recursor functionality. Querying a domain other than `NIP_DOMAIN` will result failure. You need supplemental DNS recursors.

**No configuration**

```
$ docker run --rm -d -p 53:53/udp registry.gitlab.com/warheadsse/pdns-nipio:latest
$ dig @localhost derp.lcl.io
derp.lcl.io.             432000  IN      A       127.0.0.1
$ dig @localhost thing.10.0.0.1.lcl.io
thing.10.0.0.1.lcl.io.   432000  IN      A       10.0.0.1
```

**Domain Configured**

```
$ docker run --rm -d -e NIP_DOMAIN=exam.pl -p 53:53/udp registry.gitlab.com/warheadsse/pdns-nipio:latest
$ dig @localhost derp.exam.pl
derp.exam.pl.             432000  IN      A       127.0.0.1
$ dig @localhost thing.10.0.0.1.exam.pl
thing.10.0.0.1.exam.pl.   432000  IN      A       10.0.0.1
```

**IP Configured**

```
$ docker run --rm -d -e NIP_DEFAULT_ADDRESS=192.168.99.100 -p 53:53/udp registry.gitlab.com/warheadsse/pdns-nipio:latest
$ dig @localhost derp.lcl.io
derp.lcl.io.             432000  IN      A       192.168.99.100
$ dig @localhost thing.10.0.0.1.lcl.io
thing.10.0.0.1.lcl.io.   432000  IN      A       10.0.0.1
```

## How it is built

This container is built on top of `alpine`, custom compiles [PowerDNS][] to include _only_ the `pipe` backend, installs [nip.io][] software to `/nip`, and configures `pdns_server` to use `/nip/backend.py`.

See the [Dockerfile](Dockerfile) for extensive detail.


## LICENSE

This repository is licensed under [The Unlicense](http://unlicense.org)


[PowerDNS]: https://www.powerdns.com
[NIP.IO]: http://nip.io
[nipi.io]: https://xp-dev.com/git/nip.io
