#!/bin/sh
set -e

# --help, --version
[ "$1" = "--help" ] || [ "$1" = "--version" ] && exec pdns_server $1

# replace the ipaddress, if $NIP_DEFAULT_ADDRESS set
if [ "x$NIP_DEFAULT_ADDRESS" != "x" ]; then
  sed -i "s/ipaddress=.*/ipaddress=$NIP_DEFAULT_ADDRESS/" /nip/backend.conf
fi

# replace the domain, if $NIP_DOMAIN set
if [ "x$NIP_DOMAIN" != "x" ]; then
  sed -i "s/lcl.io/$NIP_DOMAIN/g" /nip/backend.conf
fi

# Setup regex, so superfluous queries don't get passed to backend
NIP_DOMAIN=$(echo -n ${NIP_DOMAIN-lcl.io} | sed 's/\./\\./')
PIPE_REGEX="^.*\.${NIP_DOMAIN}$"

# Run pdns server
trap "pdns_control quit" SIGHUP SIGINT SIGTERM

pdns_server --no-config --launch=pipe --pipe-command=/nip/backend.py --pipe-abi-version=1 --pipe-regex="$PIPE_REGEX" "$@" &

wait
